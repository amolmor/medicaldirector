﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Web.Http.Results;
using Newtonsoft.Json;
using MedicalDirector.Models;
using MedicalDirector.Services;

namespace MedicalDirector.API.Controllers.Tests
{
    [TestClass()]
    public class PatientGroupsControllerTests
    {
        [TestMethod()]
        public void Calculate_WhenPatientsWith5GroupsPassed_ShouldRetern5()
        {
            var expected = 5;
            var matrix = @"{
                                       'matrix':
                                       [
                                       [1, 0, 1, 1, 1],
                                       [1, 0, 0, 0, 0],
                                       [1, 0, 0, 0, 1],
                                       [0, 0, 1, 0, 0],
                                       [0, 1, 0, 0, 0],
                                       [0, 1, 0, 0, 1]
                                       ]
                                       }";
            var jObj = JsonConvert.DeserializeObject<PatientGroupModel>(matrix);
            var _businessLogicRepository = new PatientGroupService();
            var controller = new PatientGroupsController(_businessLogicRepository);
            var response = controller.Calculate(jObj);
            var result = response as OkNegotiatedContentResult<Patient>;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(expected, result.Content.NumberOfGroups);
        }

        [TestMethod()]
        public void Calculate_WhenNoPatientsDataPassed_ShouldReturnRequestError()
        {
            var expected = "Please provide valid patient data.";
            var matrix = String.Empty;
            var jObj = JsonConvert.DeserializeObject<PatientGroupModel>(matrix);
            var _businessLogicRepository = new PatientGroupService();
            var controller = new PatientGroupsController(_businessLogicRepository);
            var response = controller.Calculate(jObj);
            var result = response as BadRequestErrorMessageResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Message);
            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod()]
        public void Calculate_WhenPatientsWithNonStandardValuePassed_ShouldReturnRequestError()
        {
            var expected = "Patient data must contain only 1 or 0.";
            var matrix = @"{
                                       'matrix':
                                       [
                                       [1, 0, -1, 1, 1],
                                       [1, 0, 0, 0, 0],
                                       [1, 0, 0, 0, 1],
                                       [0, 0, 1, 0, 0],
                                       [0, 1, 0, 0, 0],
                                       [0, 1, 0, 0, 1]
                                       ]
                                       }";
            var jObj = JsonConvert.DeserializeObject<PatientGroupModel>(matrix);
            var _businessLogicRepository = new PatientGroupService();
            var controller = new PatientGroupsController(_businessLogicRepository);
            var response = controller.Calculate(jObj);
            var result = response as BadRequestErrorMessageResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Message);
            Assert.AreEqual(expected, result.Message);
        }

        [TestMethod()]
        public void Calculate_WhenPatirntsWithExcessiveValuePassed_ShouldReturnRequestError()
        {
            var expected = "Patient data too big. Please provide matrix less than 50 x 50.";
            var matrix = @"{
                                       'matrix':
                                       [
                                       [1, 0, 1, 1, 1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                                       [1, 0, 0, 0, 0],
                                       [1, 0, 0, 0, 1],
                                       [0, 0, 1, 0, 0],
                                       [0, 1, 0, 0, 0],
                                       [0, 1, 0, 0, 1]
                                       ]
                                       }";
            var jObj = JsonConvert.DeserializeObject<PatientGroupModel>(matrix);
            var _businessLogicRepository = new PatientGroupService();
            var controller = new PatientGroupsController(_businessLogicRepository);
            var response = controller.Calculate(jObj);
            var result = response as BadRequestErrorMessageResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Message);
            Assert.AreEqual(expected, result.Message);
        }
    }
}