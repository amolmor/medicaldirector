﻿using MedicalDirector.Models;
using MedicalDirector.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MedicalDirector.API.Controllers
{
    public class PatientGroupsController : ApiController
    {
        private IPatientGroupService _patientGroupService;
        public PatientGroupsController(IPatientGroupService patientGroupService)
        {
            _patientGroupService = patientGroupService;
        }

        /// <summary>
        /// Calcuate the no of groups of patient in the given matrix of position of patients.
        /// </summary>
        /// <param name="matrix">A matrix of 1 and 0's that shows posistion of infected patients.</param>
        /// <returns>No of groups of patients in format '{numberOfGroups: 4}'</returns>
        [HttpPost]
        [Route("api/patient-groups/calculate")]
        public IHttpActionResult Calculate([FromBody]  PatientGroupModel matrix)
        {
            
            if (matrix == null || !ModelState.IsValid)
                return BadRequest("Please provide valid patient data.");
            
            List<List<int>> patientPositionsMatrix = matrix.Matrix;

            
            if (patientPositionsMatrix.Where(x=>x.Any(y=> y <0 || y > 1 )).Count() > 0)
            {
                return BadRequest("Patient data must contain only 1 or 0.");
            }

            
            if (patientPositionsMatrix.Count > 50 || patientPositionsMatrix[0].Count > 50)
            {
                return BadRequest("Patient data too big. Please provide matrix less than 50 x 50.");
            }

            Patient result = _patientGroupService.CalculateTotalNoOfPatientGroups(patientPositionsMatrix);

            return Ok(result);
        }

        [HttpGet]
        public string Get()
        {
            return "V1.0";
        }
    }
}
