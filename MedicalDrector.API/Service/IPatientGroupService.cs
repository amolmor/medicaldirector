﻿using System.Collections.Generic;
using MedicalDirector.Models;

namespace MedicalDirector.Services
{
    public interface IPatientGroupService
    {
        Patient CalculateTotalNoOfPatientGroups(List<List<int>> patientPositionsMatrix);
        void FindPatientGroups(List<List<int>> patientPositionsMatrix, bool[,] processed, int row, int column);
        bool OkToProcessData(List<List<int>> patientPositionsMatrix, bool[,] processed1, int row, int column);
    }
}
