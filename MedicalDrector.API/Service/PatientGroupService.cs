﻿using System.Collections.Generic;
using MedicalDirector.Models;

namespace MedicalDirector.Services
{
    public class PatientGroupService : IPatientGroupService
    {

        private readonly List<int> _rowNumber = new List<int> { -1, -1, -1, 0, 0, 1, 1, 1 };
        private readonly List<int> _colNumber = new List<int> { -1, 0, 1, -1, 1, -1, 0, 1 };

        /// <summary>
        /// Iterate through the patient position matrix and calculate the number of groups
        /// </summary>
        /// <param name="patientPositionsMatrix"> Pass the patient positon matrix wich contains 1 and 0's.</param>
        /// <returns>A object of type 'Return' whcih has Number of groups values.</returns>
        public Patient CalculateTotalNoOfPatientGroups(List<List<int>> patientPositionsMatrix)
        {
            int rows = patientPositionsMatrix.Count;
            int columns = patientPositionsMatrix[0].Count;
            bool[,] processed = new bool[rows, columns];
            int countOfGroups = 0;
            for (int i = 0; i < rows; i++) 
            {
                for (int j = 0; j < columns; j++)
                {
                    if (patientPositionsMatrix[i][j] == 1 && !processed[i, j])
                    {
                        FindPatientGroups(patientPositionsMatrix, processed, i, j);// Pass it to function for finding all the 1 in same group and mark them as processed.
                        countOfGroups++;
                    }

                }
            }

            return new Patient() { NumberOfGroups = countOfGroups };
        }

        /// <summary>
        /// This function finds 1s immediatly next to the position passed to it. This function calls itself ,unless it finds all the 1's next to each other. Thats one group of patient.
        /// </summary>
        /// <param name="patientPositionsMatrix">The patient position matrix.</param>
        /// <param name="processed">Variable that holds if the data is processed for all positions</param>
        /// <param name="row">Row of currunt data to be processed in the patientPostionsMatrix.</param>
        /// <param name="column">Column of currunt data to be processed in the patientPostionsMatrix.</param>
        public void FindPatientGroups(List<List<int>> patientPositionsMatrix, bool[,] processed, int row, int column)
        {
            processed[row, column] = true;
            for (int i = 0; i < 8; i++)
            {
                //we are checking all the positions surrounding the currunt position for 1's and processing them.
                if (OkToProcessData(patientPositionsMatrix, processed, row + _rowNumber[i], column + _colNumber[i]))
                {
                    FindPatientGroups(patientPositionsMatrix, processed, row + _rowNumber[i], column + _colNumber[i]);
                }
            }
        }

        /// <summary>
        /// Pass the Row and Column number of the position that we want to check if they can be processed.
        /// </summary>
        /// <param name="patientPositionsMatrix">The patient position matrix.</param>
        /// <param name="row">Row of currunt data to be processed in the patientPostionsMatrix.</param>
        /// <param name="column">Column of currunt data to be processed in the patientPostionsMatrix.</param>
        /// <param name="processed">Variable that holds if the data is processed for all positions</param>
        /// <returns>Returns boolean value.</returns>
        public bool OkToProcessData(List<List<int>> patientPositionsMatrix, bool[,] processed, int row, int column)
        {
            
            return (row >= 0) && (row < patientPositionsMatrix.Count) && (column >= 0) && (column < patientPositionsMatrix[0].Count) && patientPositionsMatrix[row][column] == 1 && !processed[row, column];
        }
    }
}
