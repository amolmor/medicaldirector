﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace MedicalDirector.Models
{
    public class PatientGroupModel
    {
        [Required]
        public List<List<int>> Matrix { get; set; }
    }
}
